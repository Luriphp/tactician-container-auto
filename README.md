## Tactician Container plugin

**This plugin allows lazy loading CommandHandlers from a [`PSR-11`](https://github.com/php-fig/container) container.**

**This plugin add an autofind functionnality to league/tactician-container plugin. It's require league/tactician-container **

This functionnality is usefull to add more command without change of your bus configuration.

## Install

Via Composer

``` bash
$ composer require luri/tactician-container-auto
```


## Usage

This plugin is based onto league/tactician-container.
Documentation for tactician-container is available on the [official website](http://tactician.thephpleague.com/plugins/container/).

for autofind handler functionnality work, you must follow this name convention : 
Handler class must be named : {CommandClassName}Handler.
And this class must be known of your PSR11 container.

For example : 
Command : AddTrampoline
Command Handler : AddTrampolineHandler

Setup can be a little more simple than tactician-container : 
( this example is adpated from official example](http://tactician.thephpleague.com/plugins/container/) )

```php

// Create a new Tactician ContainerLocator, passing a fully configured container instance.
use Luri\Tactician\ContainerLocatorWithAutofind;
$containerLocator = new ContainerLocatorWithAutofind($container);

// Finally, we pass the ContainerLocator into the CommandHandlerMiddleware that
// we use in almost every CommandBus.
$commandHandlerMiddleware = new CommandHandlerMiddleware(
    new ClassNameExtractor(),
    $containerLocator,
    new HandleInflector()
)

// And that's it! Drop it in our command bus and away you go.
$commandBus = new CommandBus(
    [
        // your other middlewares...
        $commandHandlerMiddleware,
    ]
);
```

## Testing

No test for now. To be continued...

## Credits

- [Nigel Greenway](https://github.com/NigelGreenway)
- [Jonathan Bernardi](https://github.com/spekkionu)
- [Hannes Van De Vreken](https://github.com/hannesvdvreken)
- [Ross Tuck](https://github.com/rosstuck)
- [All Contributors](https://github.com/thephpleague/tactician-container/contributors)

## Security
Disclosure information can be found on [the main Tactician repo](https://github.com/thephpleague/tactician#security).

## License

The MIT License (MIT). Please see [License File](LICENSE) for more information.
