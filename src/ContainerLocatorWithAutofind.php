<?php

namespace Luri\Tactician;

use League\Tactician\Exception\MissingHandlerException;
use League\Tactician\Container\ContainerLocator;

/**
 * Fetch handler instances from an in-memory collection.
 *
 * This locator allows you to bind a handler FQCN to receive commands of a
 * certain command name.
 *
 * This locator add an aufind handler fonctionnality to League/tactician-container plugin.
 * This work by simply add "Handler" to command class name.
 *
 * Example : if command is named "AddTrampoline". Command Handler must be named "AddTrampolineHandler"
 */
class ContainerLocatorWithAutofind extends ContainerLocator
{
    /**
     * Retrieves the handler for a specified command
     *
     * @param string $commandName
     *
     * @return object
     *
     * @throws MissingHandlerException
     */
    public function getHandlerForCommand($commandName)
    {
        if (isset($this->commandNameToHandlerMap[$commandName])) {
			//Manual map
			$serviceId = $this->commandNameToHandlerMap[$commandName];
		} else {
			//Autofind handler fonctionnality
			$serviceId = $commandName . 'Handler';
		}

		//Send exception if handler is not know by container
		if (! $this->container->has($serviceId)) {
            throw MissingHandlerException::forCommand($commandName);
        }


        return $this->container->get($serviceId);
    }
}
